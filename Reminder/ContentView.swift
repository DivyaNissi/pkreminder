//
//  ContentView.swift
//  Reminder
//
//  Created by divya kothagattu on 24/09/20.
//  Copyright © 2020 divya kothagattu. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    @State var searchText: String = ""
    var body: some View {
        VStack {
            SearchBar(text: searchText)
        VStack(alignment: .center, spacing: 5) {
            HStack(spacing: 4) {
                MenuView(image: "calendar.circle.fill", title: "Today", count: "6")
                
                MenuView(image: "clock.fill", title: "Scheduled", count: "5")
            }
            HStack(spacing: 4) {
                MenuView(image: "tray.fill", title: "All", count: "12")
                MenuView(image: "flag.circle.fill", title: "Flagged", count: "1")
                }
            }
            Spacer()

            HStack {
                Text("My Lists")
                    .font(.title)
                    .bold()
                Spacer()
            }.padding(.all, 8.0)
            
                VStack(spacing: 0) {
                    ListView(icon: "briefcase.fill", title: "Jobs", count: "1")
                    ListView(icon: "car.fill", title: "Car", count: "2")
                    ListView(icon: "dollarsign.circle.fill", title: "Money", count: "6")

                } .cornerRadius(8.0)
            Spacer()
            HStack {
                Spacer()
                Button(action: {
                    
                },label: {
                    Text("Add List")
                    .bold()
                    })
            }.padding()
             .background(Color.gray.opacity(0.1))

        }.padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

