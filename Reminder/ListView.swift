//
//  ListView.swift
//  Reminder
//
//  Created by divya kothagattu on 24/09/20.
//  Copyright © 2020 divya kothagattu. All rights reserved.
//

import SwiftUI

struct ListView: View {
    var icon: String
    var title: String
    var count: String

    var body: some View {
        HStack {
         Image(systemName: icon)
         Text(title)
         Spacer()
         Text(count)
         Image(systemName: "chevron.right")
        }
        .padding()
        .background(Color.gray.opacity(0.1))
    }
}

struct ListView_Previews: PreviewProvider {
    static var previews: some View {
        ListView(icon: "briefcase.fill", title: "Jobs", count: "1")
        .previewLayout(.sizeThatFits)
    }
}


