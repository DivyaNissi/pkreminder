//
//  MenuView.swift
//  Reminder
//
//  Created by divya kothagattu on 24/09/20.
//  Copyright © 2020 divya kothagattu. All rights reserved.
//

import SwiftUI

struct MenuView: View {
    var image: String
    var title: String
    var count: String

    var body: some View {
        VStack {
            HStack {
                Image(systemName: image)
                Spacer()
                Text(count)
            }
            .font(.largeTitle)
            HStack {
                Text(title)
                Spacer()
            }.font(.title)
        }.padding()
        .background(Color.gray.opacity(0.1))
    }
}

struct MenuView_Previews: PreviewProvider {
    static var previews: some View {
        MenuView(image: "calendar.circle.fill", title: "Today", count: "6")
        .previewLayout(.sizeThatFits)
    }
}
