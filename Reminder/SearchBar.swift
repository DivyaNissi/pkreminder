//
//  SearchBar.swift
//  Reminder
//
//  Created by divya kothagattu on 24/09/20.
//  Copyright © 2020 divya kothagattu. All rights reserved.
//

import SwiftUI


struct SearchBar: View {
    @State var text: String = ""
    var body: some View {
        HStack {
            Image(systemName: "magnifyingglass")
            TextField("Search", text: $text)
        }.padding(4)
         .background(Color.gray.opacity(0.1))
         .cornerRadius(8.0)
    }
}

struct SearchBar_Previews: PreviewProvider {
    static var previews: some View {
        SearchBar()
    }
}
